/*
 * Class contenant toutes les methodes relatives aux equipes
 */
package championatminifoot;

import static championatminifoot.Tableaux.permuter;
import java.util.Random;
//import java.util.regex.*;


/**
 *
 * @author Somboom TUNSAJAN (G3)
 */
public class Equipe {

    /* Permet de demander le nombre d equipe souhaitées dans le championat
     * @input: void
     * @return int nbrEquipe
     */
    public static int setNbrEquipe() {
        int nbrEquipe = -1;
        boolean flag = false;
        do {
            try {
                nbrEquipe = Integer.parseInt(nhpack.Console.readLine("Combien d'equipe à encoder ? (doit etre pair): "));
                if (nbrEquipe <= 0) {
                    System.err.println("Nombre incorrect (<=0) ");
                } else if (nbrEquipe % 2 != 0) {
                    System.err.println("Le nombre d'equipe doit etre pair !");
                } else {
                    flag = true;
                }
            } catch (NumberFormatException e) {
                System.err.println("is not a number ! (" + e.getMessage() + ")"); /* Si pas un int */

            }
        } while (!flag);
        return nbrEquipe;
    }
    /* Permet d'encoder la liste des equipes pour le championat
     * @input nbrEquipe
     * @return String listEquipe
     */

    public static String[] setListEquipe(int nbrEquipe) {
        String listEquipe[] = new String[nbrEquipe];

        for (int i = 0; i < listEquipe.length; i++) {
            System.out.print("Equipe " + (i + 1) + ": ");
            listEquipe[i] = setEquipe(listEquipe);
        }
        /* On melange */
        Random r=new Random();
        for(int i=0; i<listEquipe.length; i++){
            permuter(listEquipe, i,(int)r.nextInt(listEquipe.length-1));
        }
        return listEquipe;
    }

    /* Permet d'encoder une equipe dans la liste tout en
     * verifiant que l equipe n est pas deja dans la liste
     * @param String[] listEquipe contient les equipes deja encodées
     * @return String une equipe valide
     */
    public static String setEquipe(String[] listEquipe) {

        // Matcher match;
        String element;
        boolean flag = true;

        do {
            element = nhpack.Console.readLine();
           // match = regex.matcher(element);

            /*  if (!Pattern.compile("").matcher(element).find()){
     
             System.err.println("Erreur format");
             } else */
            if (searchEquipe(listEquipe, element) != -1) {
                System.err.println("Equipe dejà encodé !");
            } else {
                flag = false;
            }
        } while (flag);
        return element;
    }
    /* Affiche la liste des equipes
     * @param String[] listEquipe liste à afficher
     * @return void
     */

    public static void afficherListEquipe(String[] listEquipe) {
        for (String nomEquipe : listEquipe) {
            System.out.println("* " + nomEquipe);
        } 
    }
    /* Permet de selectionner une equipe dans la liste
     * @param String[] listEquipe cotient la liste des equipes
     * @return int l indice de l equipe choisie;
     */

    public static int choixEquipe(String[] listEquipe) {
        boolean flag;
        int choix = 0;
        do {
            flag = false;
            choix = searchEquipe(listEquipe, nhpack.Console.readLine("Recherche: "));
            System.out.println();
            if (choix == -1) {
                System.err.println("Equipe non trouvée !");
                flag = true;
            }
        } while (flag);

        return choix;
    }
    /* Affiche les matchs de l equipe selectionée
     * @param String championat[][][] le championat
     *        String equipe l equipe dont les matchs doivent être affichés
     * @return void
     */

    public static void afficherCalandrierEquipe(String championat[][][], int[][][] resultat, String equipe) {
        for (int indexDay = 0; indexDay < championat.length; indexDay++) { /* journée */

            for (int indexMatch = 0; indexMatch < championat[indexDay].length; indexMatch++) { /* matchs de la journée */
                /* on affiche les matchs de l equipe concernée */

                if ((championat[indexDay][indexMatch][0].equalsIgnoreCase(equipe))
                        || championat[indexDay][indexMatch][1].equalsIgnoreCase(equipe)) {
                    if (resultat[indexDay][indexMatch][0] != -1) {
                        System.out.println(championat[indexDay][indexMatch][0] + " " + resultat[indexDay][indexMatch][0] + " - " + resultat[indexDay][indexMatch][1] + " " + championat[indexDay][indexMatch][1]);
                    } else {
                        System.out.println(championat[indexDay][indexMatch][0] + " - " + championat[indexDay][indexMatch][1]);
                    }
                }
            }
        }
    }
    /* Recherche une equipe dans la liste des participants
     * @param String[] list contient les equipes
     *        String element element à rechercher
     * @return int l indice de l element si touvé
     *             -1 si non trouvé
     */

    public static int searchEquipe(String[] list, String element) {
        int indice = 0;
        while (indice < list.length && list[indice] != null) { /* Si la liste est pas remplie */

            if (list[indice].compareToIgnoreCase(element) == 0) {
                return indice;
            }
            indice++;
        }
        return -1;
    }
}
