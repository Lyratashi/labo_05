/*
 * class regroupant toutes les methodes concernant
 * les matchs de foot
 */
package championatminifoot;

import static championatminifoot.Tableaux.*;
/**
 *
 * @author Somboom TUNSAJAN (G3)
 */
public class GestionDesMatch {

    /* Affiche une journée de championat 
     * @param String[][] la journée à afficher
     * @return boolean si tout c est bien passé
     */

    public static boolean afficherJournee(String[][] matchOfDay, int[][] resultat) {

        if (matchOfDay == null) { /* Si journée vide */

            return false;
        }
        for (int i = 0; i < matchOfDay.length; i++) {
            if (resultat[i][0] != -1) { /* Resultat dispo */

                System.out.println(matchOfDay[i][0] + " " + resultat[i][0] + " - " + resultat[i][1] + " " + matchOfDay[i][1]);

            } else {
                System.out.println(matchOfDay[i][0] + " - " + matchOfDay[i][1]);
            }
        }
        return true;
    }
    /* Genere le championat
     * @param String[] listEquipe equipe participant au championat
     * @return String[day][match][equipe]
     */

    public static String[][][] genererChampionat(String[] listEquipe) {
        int indexDay = listEquipe.length - 1; /* p-1 */

        int indexMatch = listEquipe.length / 2; /* nombre de match par jour */

        String championat[][][] = new String[indexDay][indexMatch][2]; /* il faut deux equipe pour faire un match */

        for (int i = 0; i < indexDay; i++) {
            championat[i] = genererJournee(listEquipe);
        }
        return championat;
    }

    /* Affiche le championat (match) + resultats si disponible
     * @param String championat[][][] championat à afficher int[][][] resultat
     * @return void
     */
    public static void afficherChampionat(String championat[][][], int resultat[][][]) {
        for (int i = 0; i < championat.length; i++) {
            System.out.println("Journée " + (i + 1));
            afficherJournee(championat[i], resultat[i]);
            System.out.println();
        }
    }
}
