/*
 * Class contenant les methode permettant des operations
 * sur un tableau
 */
package championatminifoot;

/**
 *
 * @Somboom TUNSAJAN (G3)
 */
public class Tableaux {
    /* Permet de decaler un tableau a une dimenssion
     * @param String[] tableauADecaler
     * @return boolean verifie si tout c est bien passé
     */

    public static boolean decallageTableau(String[] tableauADecaler) {
        if (tableauADecaler == null) {
            return false; /* pas de données dans le tableau */

        }
        String dernierElement = tableauADecaler[tableauADecaler.length - 1]; /* On sauvegarde le dernier element */

        for (int i = tableauADecaler.length - 1; i > 0; i--) {
            tableauADecaler[i] = tableauADecaler[i - 1]; /* On decale vers la droite */

        }
        tableauADecaler[0] = dernierElement;
        return true;
    }
    /* Permet de permuter deux element d un tableau à une dimension
     * (On considere que les element sont present dans le tableau)
     * @param String[] arrayString dont on doit permuter 2 elements
     *        int indiceAPermuterG premier element
     *        int indiceAPermuterD deuxieme element a permuter
     * @return boolean si tout c est bien passé
     */

    public static boolean permuter(String[] arrayString, int indiceAPermuterG, int indiceAPermuterD) {
        /* Parametres valides ? */
        if (arrayString == null
           || arrayString[indiceAPermuterG] == null
           || arrayString[indiceAPermuterD] == null
           || indiceAPermuterG == indiceAPermuterD) {
            return false;
        }
        String tmp = arrayString[indiceAPermuterG]; /* on sauvegarde l element de gauche */
        arrayString[indiceAPermuterG] = arrayString[indiceAPermuterD];
        arrayString[indiceAPermuterD] = tmp;
        return true;
    }
    /* Permet de permuter deux element d un tableau à une dimension
     * (On considere que les element sont present dans le tableau)
     * @param String[] tableau dont on doit permuter 2 elements
     *        int indiceAPermuterG premier element
     *        int indiceAPermuterD deuxieme element a permuter
     * @return void
     */

    public static void permuter(int[][] arrayInt, int indiceG, int indiceD) {
        int[] tmp = arrayInt[indiceG];
        arrayInt[indiceG] = arrayInt[indiceD];
        arrayInt[indiceD] = tmp;
    }
    /*
     * Trie le tableau[][] passer en parametre en fonction de la deuxieme colonne
     * (TriBulle)
     * @param int[][] array le tableau à trier
     * @return void
     */

    public static void triDecroissant(int[][] array) {
        /* on prends chaqu element que l on compare au tableau */
        for (int j = 0; j < array.length; j++) {
            for (int i = 0; i < array.length; i++) {
                if (array[j][1] > array[i][1]) { /* Si elment en cours > on permute */
                    permuter(array, i, j);
                }
            }
        }
    }
        /* Genere une journée complete de championat 
     * @param String[] listEquipe liste des equipes participant à la journée
     * @return String[][] tableau contenant les matchs générés
     */
    public static String[][] genererJournee(String[] listEquipe) {

        String matchs[][] = new String[listEquipe.length / 2][2]; /* allocation des matchs de la journée */

        decallageTableau(listEquipe);
        permuter(listEquipe, 0, 1);
        /* On mets les matchs comme demandé dans l ennoncé */
        for (int j = 0; j < matchs.length; j++) {
            matchs[j][0] = listEquipe[j];
            matchs[j][1] = listEquipe[listEquipe.length - 1 - j];
        }
        return matchs;
    }
}
