/*
 * Labo 5: Gestion d un championat de mini foot
 * Ce programme permet:
 * - Encoder des participants
 * - Afficher les matchs de la prochaine journée
 * - Afficher les matchs d une equipe
 * - Afficher la liste des participants
 * - Encoder des resultats
 * - Afficher classement championat
 */
package championatminifoot;
/**
 *
 * @author Somboom TUNSAJAN (G3)
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ChampionatMiniFoot.run();
    } 
}