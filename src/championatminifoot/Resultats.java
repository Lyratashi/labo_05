/*
 * Class Resultats: contient toutes les methodes permettant de 
 * gerer les resultats

 */
package championatminifoot;

import static championatminifoot.Equipe.*;
import static championatminifoot.Tableaux.*;

/**
 *
 * @Somboom TUNSAJAN (G3)
 */
public class Resultats {

    public final static int VICTOIRE = 3;
    public final static int DRAW = 1;

    /* Genere les resultats par defaut
     * @param int nb journee, int nbMatchByDay
     * @return int[][][] tableau de int avec les resultats init a -1
     */
    public static int[][][] genererTabResultat(int nbJournee, int nbMatchByDay) {
        int[][][] resultat = new int[nbJournee][nbMatchByDay][2];
        for (int i = 0; i < nbJournee; i++) {
            for (int j = 0; j < nbMatchByDay; j++) {
                resultat[i][j][0] = -1; /* Equipe 1 */
                resultat[i][j][1] = -1; /* Equipe 2 */
            }
        }
        return resultat;
    }
    /*
     * Permet d encoder les resultat d une journee
     * @param String[][] journee contient les matchs pour leesquels ont veux encoder les resultats
     *        int[][] classement contient le classement par equipe
     *        String[] listEquipe contient les equipes participant au championat
     * @return int[][] contient les resultats encodes
     */

    public static int[][] encoderResultatJournee(String[][] journee, int[][] classement, String listEquipe[]) {
        int[][] resultatJournee = new int[journee.length][2];
        for (int i = 0; i < journee.length; i++) {
            System.out.println("Match " + (i + 1) + "\n-> " + journee[i][0] + "-" + journee[i][1]);
            System.out.print(journee[i][0] + ": ");
            resultatJournee[i][0] = setScore(); /* equipe 1*/

            System.out.print(journee[i][1] + ": ");
            resultatJournee[i][1] = setScore(); /* equipe 2 */

            if (resultatJournee[i][0] > resultatJournee[i][1]) {
                setPoint(searchEquipe(listEquipe, journee[i][0]), classement, VICTOIRE);
            } else if (resultatJournee[i][0] < resultatJournee[i][1]) {
                setPoint(searchEquipe(listEquipe, journee[i][1]), classement, VICTOIRE);
            } else {
                setPoint(searchEquipe(listEquipe, journee[i][0]), classement, DRAW);
                setPoint(searchEquipe(listEquipe, journee[i][1]), classement, DRAW);
            }
        }
         /* on mets a jour le classement */
        return resultatJournee;
    }
    /* 
     * Permet d encoder un score pour une equipe
     * @param void
     * @return int le score encodé
     */

    public static int setScore() {

        int score = -1;
        boolean flag = true;
        do {
            try {
                score = Integer.parseInt(nhpack.Console.readLine());
                if (score < 0) {
                    System.out.println("Ne peut etre negatif !");
                } else {
                    flag = false;
                }
            } catch (NumberFormatException e) {
                System.err.println("Data incorrect ! " + e.getMessage());
            }
        } while (flag);
        return score;
    }
    /* 
     * Ajoutes un nombre de points à un equipe
     * @param int idEquipe contient l id de l equipe à laquelle il faut rajouté des points
     *        int[][] classement contient le classement par equipe
     *        int szPoint contient le nombre de points à ajouter
     * @return void
     */

    public static void setPoint(int idEquipe, int classement[][], int szPoint) {
        int i = 0;
        while (classement[i][0] != idEquipe) { /* on cherche l equipe dans le classement */
            i++;
        }
        classement[i][1] += szPoint; /* on ajoute les points */
    }
    /*
     * Initialise le classement par defaut 0
     * @param int nbEquipe nombre de participants
     * @return int[][] le classsement initialiser
     */

    public static int[][] genererClassement(int nbEquipe) {
        int[][] classement = new int[nbEquipe][2];
        for (int i = 0; i < classement.length; i++) {
            classement[i][0] = i; /* idEquipe */
            classement[i][1] = 0; /* tout le monde commence a 0 points */
        }
        return classement;
    }
    /*
     * Permet d afficher le classement par equipe
     * @param String[] listEquipe contient le nom des equipes
     *        int[][] classement contient le classement 
     * @return void
     */

    public static void afficherClassement(String[] listEquipe, int[][] classement) {
        triDecroissant(classement);
        for (int i = 0; i < classement.length; i++) {
            System.out.println(listEquipe[classement[i][0]] + " " + classement[i][1]);
        }

    }
}
