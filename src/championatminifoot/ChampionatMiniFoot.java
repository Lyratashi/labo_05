/*
 * Class Applcation contient les methodes permettant de lancer 
 * l application ainsi que la gestion du menu
 */
package championatminifoot;

import static championatminifoot.Equipe.*;
import static championatminifoot.GestionDesMatch.*;
import static championatminifoot.Resultats.*;

/**
 *
 * @author Somboom TUNSAJAN (G3)
 */
public class ChampionatMiniFoot {
    /* Methode lancant le "thread" principal
     * @param void
     * @return void
     */
final static int QUITTER=7;
    public static void run() {
        String[] listEquipe = null;
        String[][][] calandarChampionat = null;
        int[][][] tabResultat = null;
        int[][] classement = null;
        boolean firstTime = true;
        int choix;
        do {
            if (firstTime) { /* premier tour de boucle */

                afficherMenu(0);
            } else {
                if (listEquipe != null) {
                    afficherMenu(listEquipe.length);
                }
            }
            System.out.print("-> ");
            if (firstTime) {
                choix = setChoix(2);
                if (choix == 2) {
                    choix = QUITTER;
                }
            } else {
                choix = setChoix(QUITTER);
            }
            switch (choix) {
                case 1: /* Encoder equipe */

                    if (!firstTime) {
                        System.err.println("Cette operation efface les anciennes données !");
                        System.out.println("Etes vous sur de vouloir faire cette operation ?");
                        System.out.print("[1]oui/[2]non -> ");
                        if (setChoix(2) == 2) {
                            break; /* On ne change rien */
                        }
                    }
                    listEquipe = setListEquipe(setNbrEquipe()); /* On preserve la liste initiale */

                    calandarChampionat = genererChampionat(listEquipe);
                    tabResultat = genererTabResultat(listEquipe.length - 1, listEquipe.length / 2);
                    classement = genererClassement(listEquipe.length);
                    break;
                case 2: /* Afficher liste equipe */

                    afficherListEquipe(listEquipe);
                    break;
                case 3: /* Afficher championat */

                    afficherChampionat(calandarChampionat, tabResultat);
                    break;
                case 4: /* Afficher journée d une equipe */

                    afficherListEquipe(listEquipe);
                    System.out.println();
                    afficherCalandrierEquipe(calandarChampionat, tabResultat, listEquipe[choixEquipe(listEquipe)]);
                    System.out.println();
                    break;
                case 5: //Encoder resultat pour une journé"
                    System.out.println("Quelle journee voulez vous remplir [" + (listEquipe.length-1) + "] ?");
                    int day = setChoix(listEquipe.length -1);
                    if(tabResultat[day-1][0][0] !=-1){
                        System.out.println("Journée dejà encodée !");
                        break;
                    }
                    tabResultat[day-1] = encoderResultatJournee(calandarChampionat[day-1], classement, listEquipe);
                    break;
                case 6:
                    afficherClassement(listEquipe, classement);
                    break;

                default: /* Message quitter */

                    System.out.println("Merci aurevoir !");
            }
            firstTime = false;
        } while (choix != QUITTER); /* On quitte */

    }
    /* Affiche le menu si c est la premiere fois on peut seulement encoder
     * des equipes
     * @param int nbrEquipe contient le nombre d equipe encode si 0 menu restreint
     * @return void
     */

    public static void afficherMenu(int nbrEquipe) {
        System.out.println();
        System.out.printf("     MENU PRINCIPAL      \nGestion calandrirer championat mini foot\n\n");
        System.out.println("(1) Encoder une liste de participants [" + nbrEquipe + "]");
        if (nbrEquipe > 0) {
            System.out.println("(2) Afficher la liste des participants");
            System.out.println("(3) Afficher la liste des matchs");
            System.out.println("(4) Afficher le calandrier d'une equipe");
            System.out.println("(5) Encoder resultat");
            System.out.println("(6) Afficher classement");
            System.out.println("("+ QUITTER +") Quitter ");
        } else {
            System.out.printf("(2) Quitter\n");
        }
    }
    /* Recupere le choix de l user
     * @param boolean firstTime permet de savoir si c est la premiere fois
     *                          que l user fais un choix
     * @return int choix de l user
     */

    public static int setChoix(int nbChoix) {
        boolean flag = true;
        int choix = 0;
        int quitter = nbChoix;
        do {
            try {
                choix = Integer.parseInt(nhpack.Console.readLine());
                if (choix <= 0 || choix > quitter) {
                    System.out.print("Choix incorrect !\n-> ");
                } else {
                    flag = false;
                }
            } catch (NumberFormatException e) {
                System.out.print("is not a number ! (" + e.getMessage() + ")\n-> ");
            }
        } while (flag);
        return choix;
    }

}